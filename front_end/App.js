import React, { Component } from "react";
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { ScrollView } from 'react-native-gesture-handler';
import {
    Container, Header, Title, Content, Footer,
    FooterTab, Button, Left, Right, Body, Icon, Card, CardItem
} from 'native-base';
import { StatusBar, View, Alert, TouchableOpacity, ListView, FlatList, StyleSheet, Text } from "react-native";
import axios from "axios";
// import { Constants, Location } from "expo";
import * as Permissions from "expo-permissions";
import * as Location from "expo-location";
import MapView, { PROVIDER_GOOGLE, MAP_TYPES } from 'react-native-maps';
import Constants from 'expo-constants';
import { setCustomText } from 'react-native-global-props';

var serviceAccount = require("./serviceAccountKey.json");

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            latitude: null,
            longitude: null,
            datum: [],
            location: null,
            datumAvai: false,
            ipaddress: "192.168.0.135",
            placename: '',
            building: "Home",
            fontLoaded: false
        };
    };

    async componentDidMount() {
        await Font.loadAsync({
            'productsans-regular': require('./assets/fonts/ProductSans/ProductSansRegular.ttf'),
            'productsans-bold': require('./assets/fonts/ProductSans/ProductSansBold.ttf'),
            'productsans-bold-italic': require('./assets/fonts/ProductSans/ProductSansBoldItalic.ttf'),
            'productsans-italic': require('./assets/fonts/ProductSans/ProductSansItalic.ttf'),

            'muli-regular': require('./assets/fonts/Muli/Muli-Regular.ttf'),
            'muli-bold': require('./assets/fonts/Muli/Muli-Bold.ttf'),
            'muli-bold-italic': require('./assets/fonts/Muli/Muli-BoldItalic.ttf')
        }).then(() => {
            console.log("Font set in component did mount!");
            this.defaultFonts();
            this.setState({ fontLoaded: true })
        });;
    };

    defaultFonts() {
        const customTextProps = {
            style: {
                fontFamily: 'muli-regular'
            }
        };
        setCustomText(customTextProps);
    };

    toTitleCase = (str) => {
        return str.replace(
            /\w\S*/g,
            function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    };

    //Getting the GPS info from here:
    getCoordinates = (aCoord) => {
        proj4.defs(
            "EPSG:3414",
            "+proj=tmerc +lat_0=1.366666666666667 +lon_0=103.8333333333333 +k=1 +x_0=28001.642 +y_0=38744.572 +ellps=WGS84 +units=m +no_defs"
        );
        let coord = proj4("EPSG:3414").inverse({
            x: parseFloat(aCoord.x),
            y: parseFloat(aCoord.y)
        });
        let toReturn = {
            x: coord.y,
            y: coord.x
        };
        return toReturn;
    };


    findCurrentLocation = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                const latitude = JSON.stringify(position.coords.latitude);
                const longitude = JSON.stringify(position.coords.longitude);

                let coordset = {
                    x: latitude,
                    y: longitude
                };

                coordset = this.getCoordinates(coordset);

                this.setState({
                    latitude: coordset.x,
                    longitude: coordset.y
                });
            },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    };

    findCurrentLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);

        if (status !== "granted") {
            this.setState({
                errorMessage: "Permission to access location was denied"
            });
        }

        let location = await Location.getCurrentPositionAsync({});
        this.setState({ location: location });
        this.setState({
            longitude: this.state.location.coords.longitude,
            latitude: this.state.location.coords.latitude
        });

        //Getting the results from localhost:
        axios
            .post("http://" + "10.175.7.162" + ":3000/putLongLat", {
                x: this.state.latitude,
                y: this.state.longitude
            })
            .then(response => {
                this.setState({ datum: response.data });

            })
            .catch(function (error) {
                console.warn(error);
            });

        //Getting the results from FREE GLOBAL GPS DATABASE:
        let apikey = 'ec29ab93c4824422a45fa69b26950762';
        let urlToFetch = 'https://api.opencagedata.com/geocode/v1/json?q=+'
            + this.state.latitude
            + '%2C+'
            + this.state.longitude
            + '&key=' + apikey;

        axios
            .get(urlToFetch)
            .then(response => {
                this.setState({ placename: response.data.results[0].formatted });
                this.setState({ building: response.data.results[0].components.suburb })
            })
            .catch(function (error) {
                console.warn(error);
            });
    };

    render() {
        let text, text2 = "";
        let toShow = [];
        if (this.state.errorMessage) {
            text = this.state.errorMessage;
        } else if (this.state.location) {
            text = JSON.stringify(this.state.location);
            text2 = "HEYYYYY" + JSON.stringify(this.state.datum);
            this.state.datumAvai = true;
            toShow = JSON.parse(JSON.stringify(this.state.datum));
        }
        console.log(this.state.datum);
        console.log("Above is the datum.");


        if (this.state.fontLoaded === false) {
            console.log("Font not loaded! :(");
            return <AppLoading />;
        } else {
            console.log("Font loaded! Congratulations");
            return (
                <Container style={[stylus.statusBar, stylus.container]}>
                    <View>
                    <StatusBar translucent backgroundColor="#c2185b" barStyle="light-content" />
                    </View>

                    <ScrollView
                        style={stylus.for_scrollview}
                        // contentContainerStyle={{ alignItems: 'center' }}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={stylus.blue_color}>
                            <TouchableOpacity onPress={this.findCurrentLocationAsync} style={{ marginTop: 30 }}>
                                <Text style={[{ fontSize: 25, color: 'white' }, stylus.goodmargin]}>Where am I?</Text>
                            </TouchableOpacity>

                            <View style={[stylus.goodmargin]}>
                                <Text style={{ color: 'white' }}>Your current position: </Text>
                                <Text style={{ fontFamily:'muli-bold', fontSize: 15, color: 'white' , marginBottom:16}}>
                                    {this.state.placename}
                                </Text>
                            </View>
                            
                            <View style={[{ backgroundColor: '#0439D9', padding: 16, alignItems:'flex-end' }]}>
                                <Text style={{ color: 'white' }}>
                                    Longitude: {this.state.longitude}
                                </Text>
                                <Text style={{ color: 'white' }}>
                                    Latitude: {this.state.latitude}
                                </Text>
                            </View>
                        </View>


                        <Text style={[{ fontSize: 20, marginVertical: 20, marginHorizontal: 16 }]}>Your nearest</Text>
                        <FlatList
                            data={this.state.datum}
                            renderItem={({ item }) => {
                                newItem = JSON.parse(JSON.stringify(item))
                                return (
                                    <Card style={[stylus.goodmargin, stylus.forcard, shadow]}>
                                        <CardItem header bordered style={[stylus.forcard]}>
                                            <Text style={stylus.setfont}>Address: {this.toTitleCase(item.address)}</Text>
                                        </CardItem>
                                        <CardItem style={[stylus.forcard]}>
                                            <Body>
                                                <Text style={stylus.setfont}>
                                                    Carpark Type: {(newItem.car_park_type.toLowerCase())}
                                                </Text>
                                                <Text style={stylus.setfont}>
                                                    Lots Available: {(newItem.lots_available)}
                                                </Text>
                                                <Text style={stylus.setfont}>
                                                    Type: {item.car_park_type.toLowerCase()}
                                                </Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                );
                            }
                            }
                        />

                    </ScrollView>



                </Container>
            );
        }

    }
}


const shadow = {
    //shadowColor: '#30C1DD',
    shadowRadius: 8,
    shadowOpacity: 0.2,
    shadowOffset: { width: 0, height: 4 }
};

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const stylus = StyleSheet.create({
    container: {
        // marginLeft: 20,
        // marginTop: Platform.OS === 'ios' ? 0 : Expo.Constants.statusBarHeight,
        backgroundColor: "#f1f0f1"
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
        backgroundColor: "#c2185b",
    },
    goodmargin: {
        marginTop: 8, marginBottom: 8, marginLeft: 16, marginRight: 16
    },
    forcard: {
        borderRadius: 15
    },
    warning: {
        color: 'red'
    },
    setfont: {
    },
    for_scrollview: {
        flex: 1,
        //alignSelf: 'stretch',
    },
    blue_color: {
        backgroundColor: "#056CF2"
    }
});

export default App;
