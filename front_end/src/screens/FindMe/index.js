import React, { Component } from "react";
import {
  Container, Header, Title, Content, Footer,
  FooterTab, Button, Left, Right, Body, Icon, Text, Card, CardItem
} from 'native-base';
import { View, Alert, TouchableOpacity, ListView, FlatList } from "react-native";
import axios from "axios";
// import { Constants, Location } from "expo";
import * as Permissions from "expo-permissions";
import * as Location from "expo-location";
import MapView, {PROVIDER_GOOGLE, MAP_TYPES} from 'react-native-maps'

class FindMe extends Component {

  constructor(props) {
    super(props);
    // Assign state itself, and a default value for items
    this.state = {
      latitude: null,
      longitude: null,
      datum: [],
      location: null,
      datumAvai: false
    };
  }

  getCoordinates = (aCoord) => {
    proj4.defs(
      "EPSG:3414",
      "+proj=tmerc +lat_0=1.366666666666667 +lon_0=103.8333333333333 +k=1 +x_0=28001.642 +y_0=38744.572 +ellps=WGS84 +units=m +no_defs"
    );
    let coord = proj4("EPSG:3414").inverse({
      x: parseFloat(aCoord.x),
      y: parseFloat(aCoord.y)
    });
    let toReturn = {
      x: coord.y,
      y: coord.x
    };
    //console.log(toReturn);
    return toReturn;
  };
  

  findCurrentLocation = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        const latitude = JSON.stringify(position.coords.latitude);
        const longitude = JSON.stringify(position.coords.longitude);

        let coordset = {
          x: latitude,
          y: longitude
        };

        coordset = this.getCoordinates(coordset);

        this.setState({
          latitude: coordset.x,
          longitude: coordset.y
        });
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  };

  findCurrentLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);

    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied"
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location: location });
    this.setState({
      longitude: this.state.location.coords.longitude,
      latitude: this.state.location.coords.latitude
    });
    axios
      .post("http://10.175.7.162:3000/putLongLat", {
        x: this.state.latitude,
        y: this.state.longitude
      })
      .then(response => {
        //console.log("This is the response from API: ");
        this.setState({ datum: response.data });
        //console.log(this.state.datum[0]);

      })
      .catch(function (error) {
        console.warn(error);
      });
  };

  render() {
    let text, text2 = "";
    let toShow = [];
    if (this.state.errorMessage) {
      text = this.state.errorMessage;
    } else if (this.state.location) {
      text = JSON.stringify(this.state.location);
      text2 = "HEYYYYY" + JSON.stringify(this.state.datum);
      this.state.datumAvai = true;
      toShow = JSON.parse(JSON.stringify(this.state.datum));
    }
    console.log(this.state.datum);
    console.log("Above is the datum.")

    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Carpark App</Title>
          </Body>
          <Right />
        </Header>
        <TouchableOpacity onPress={this.findCurrentLocationAsync}>
          <Text style={{ fontSize: 40 }}>Where am I?</Text>
          {/* <Text> {text} </Text> */}
          {/* <Text> {JSON.stringify(this.state.datum)} </Text> */}
        </TouchableOpacity>
        <Card>
          <CardItem header bordered>
            <Text>Where are you now?</Text>
          </CardItem>
          <CardItem>
            <Body>
              <Text>
                Longitude: {this.state.longitude}
              </Text>
              <Text>
                Latitude: {this.state.latitude}
              </Text>
            </Body>
          </CardItem>
        </Card>
        <Text style={{ fontSize: 20 }}>Your nearest</Text>
        <FlatList
          data={this.state.datum}
          renderItem={({ item }) => {
            newItem = JSON.parse(JSON.stringify(item))
            return (
              <Card style={{marginTop:10,marginBottom:10, marginLeft:10, marginRight:10}}>
              <CardItem header bordered>
                <Text>Address: {item.address}</Text>
              </CardItem>
              <CardItem>
                <Body>
                <Text>
                    Carpark Type: { (newItem.car_park_type) }
                  </Text>
                  <Text>
                    Lots Available: { (newItem.lots_available) }
                  </Text>
                  <Text>
                    Type: {item.car_park_type}
                  </Text>
                </Body>
              </CardItem>
            </Card>
            );
          }
          }
        />


        {/* {this.state.datum.length <= 0
          ? 'NO ENTRIES YET'
          :
          <ListView
            dataSource={this.state.datum}
            renderRow={(rowData) => <Text>{rowData.address}</Text>}
          />} */}

      </Container>
    );
  }
}

export default FindMe;
