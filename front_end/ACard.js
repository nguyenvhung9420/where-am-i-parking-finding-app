import React from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity
} from "react-native";
import axios from "axios";
import * as Permissions from "expo-permissions";
import FindMe from "./src/screens/FindMe";


export default class ACard extends React.Component {
  render() {
    console.log("Hello");
    return (
      <View style={styles.container}>
        <FindMe />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
