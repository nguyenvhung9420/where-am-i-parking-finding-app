var express = require("express");
const proj4 = require("proj4");
var router = express.Router();
var dateFormat = require("dateformat");

let axios = require("axios");

router.post("/putLongLat", (req, res) => {
  let acoord = {
    x: req.body.x,
    y: req.body.y
  };
  console.log("Doing the request with following coords:");
  console.log(acoord);
  getNearestCarpark(5, acoord).then(response => {
    let testSet = response;
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(testSet));
  });
});

let getCoordinates = aCoord => {
  proj4.defs(
    "EPSG:3414",
    "+proj=tmerc +lat_0=1.366666666666667 +lon_0=103.8333333333333 +k=1 +x_0=28001.642 +y_0=38744.572 +ellps=WGS84 +units=m +no_defs"
  );
  let coord = proj4("EPSG:3414").inverse({
    x: parseFloat(aCoord.x),
    y: parseFloat(aCoord.y)
  });
  let toReturn = {
    x: coord.y,
    y: coord.x
  };
  //console.log(toReturn);
  return toReturn;
};

let getDistance = (point1, point2) => {
  let dlong = (point2.x - point1.x) ** 2;
  let dlat = (point2.y - point1.y) ** 2;
  let distance = Math.sqrt(dlong + dlat);
  //console.log("Distance = " + distance);
  return distance;
};

async function getNearestCarpark(limit, aCoord) {
  try {
    let toReturn = [];
    let allSlots = await axios.get("http://localhost:3000/getslots");
    let fs = require('async-file');
    let allInfos = await fs.readFile('./csvjson.json', 'utf8');
    allInfos = JSON.parse(allInfos);
    let coordinates = aCoord;
    console.log("This is coordinates to do: " + JSON.stringify(coordinates));
    let distance = 0.0;
    let distanceList = allInfos;
    console.log(distanceList[0]);
    let elementCoord = {};
    distanceList.forEach(element => {
      elementCoord = {
        x: element.x_coord,
        y: element.y_coord
      };
      elementCoord = getCoordinates(elementCoord);
      distance = getDistance(elementCoord, coordinates);
      element["distance_calculated"] = distance;
    });
    distanceList.sort(function (a, b) {
      return a.distance_calculated - b.distance_calculated;
    });
    for (i = 0; i < limit; i++) {
      toReturn.push(distanceList[i]);
    }
    toReturn.forEach(elementToReturn => {
      allSlots.data.forEach(elementAllSlots => {
        if (elementToReturn.car_park_no == elementAllSlots.carpark_number) {
          elementToReturn["lots_available"] = elementAllSlots["carpark_info"][0]["lots_available"];
        }
      });
    });
    return new Promise((resolve, reject) => resolve(toReturn));
  } catch (error) {
    console.error(error);
  }

}

router.get("/getcarparkinfo", (req, res) => {
  let fs = require('async-file');
  let toReturn =  fs.readFile('../csvjson.csv', 'utf8');
  toReturn = JSON.parse(toReturn);
  res.end(JSON.stringify(toReturn));

  // let api_command =
  //   "https://data.gov.sg/api/action/datastore_search?resource_id=139a3035-e624-4f56-b63f-89ae28d4ae4c";

  // axios
  //   .get(api_command + "&limit=100")
  //   .then(function (response) {
  //     //console.log(response.data.result.records);
  //     res.setHeader("Content-Type", "application/json");
  //     console.log(response.data.result.records.length + " loaded");
  //     res.end(JSON.stringify(response.data.result.records));
  //   })
  //   .catch(function (error) {
  //     console.log(error);
  //   });
});

router.get("/getslots", function (req, res) {
  //Getting API from data.gov.sg
  axios
    .get("https://api.data.gov.sg/v1/transport/carpark-availability")
    .then(function (response) {
      //console.log(response.data);
      let today = dateFormat(new Date(), "yyyy-mm-dd");
      let allRecords = JSON.parse(
        JSON.stringify(response.data.items[0].carpark_data)
      );
      let toReturn = [];
      allRecords.forEach(element => {
        if (element.update_datetime.includes(today)) {
          toReturn.push(element);
        }
      });
      res.setHeader("Content-Type", "application/json");
      console.log(response.data.items[0].carpark_data.length + " loaded");
      res.end(JSON.stringify(toReturn));
    })
    .catch(function (error) {
      console.log(error);
    });
});

module.exports = router;
